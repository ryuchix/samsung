<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GoogleController extends Controller
{
    public function getExcel() {

		$client = Google_Spreadsheet::getClient(base_path()."samsung-e24f73f792ed.json");
		// Get the file by file ID
		$file = $client->file("XXXxxxXXXXxxxXXXX");
		// Get the sheet by title
		$sheet = $file->sheet("Sheet1");
		// Flush all rows in the sheet
		var_dump($client);

    }

    public function callback(Request $request) {

    	$redirect = url('callback/auth');
    	$client_id = "680888018663-t1m25b32v4mrgr5m8ae9fnbmsu2nbikl.apps.googleusercontent.com";
    	$client_secret = "6_IaTgcjI59D5v6Liuls316Y";

    	// Authorization Link
		$authorization = "https://accounts.google.com/o/oauth2/auth?redirect_uri=".$redirect."&client_id=".$client_id."&response_type=code&scope=https://www.googleapis.com/auth/spreadsheets&approval_prompt=force&access_type=offline";

		// Authorization
		$code = $request['code'];

		// Token
		$url = "https://accounts.google.com/o/oauth2/token";
		$data = "code=".$code."&client_id=".$client_id."&client_secret=".$client_secret."&redirect_uri=".$redirect."&grant_type=authorization_code";

		// Request
		$ch = @curl_init();
		@curl_setopt($ch, CURLOPT_POST, true);
		@curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		@curl_setopt($ch, CURLOPT_URL, $url);
		@curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/x-www-form-urlencoded'
		));
		@curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = @curl_exec($ch); 
		$status_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE); 
		@curl_close($ch);
		$array = json_decode($response);

        return view('auth', [
    		'authorization' => $authorization,
    		'array' => $array
        ]);

    }

    public function sample() {
    	
    	return view('submit');

    }
}
