
require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import VueAxios from 'vue-axios';
import axios from 'axios';

import App from './App.vue';
Vue.use(VueAxios, axios);

Vue.component('team-component', require('./components/TeamComponent.vue'));

// const files = require.context('./', true, /\.vue$/i)

// files.keys().map(key => {
//     return Vue.component(_.last(key.split('/')).split('.')[0], files(key))
// })

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */




import HomeComponent from './components/HomeComponent.vue';
import TeamComponent from './components/TeamComponent.vue';
import CreateComponent from './components/CreateComponent.vue';
import IndexComponent from './components/IndexComponent.vue';
import EditComponent from './components/EditComponent.vue';
import QuestionOne from './components/QuestionOne.vue';
import QuestionTwo from './components/QuestionTwo.vue';
import QuestionThree from './components/QuestionThree.vue';
import QuestionFour from './components/QuestionFour.vue';
import QuestionFive from './components/QuestionFive.vue';
import QuestionSix from './components/QuestionSix.vue';
import EnterDetails from './components/EnterDetails.vue';
import ThankYou from './components/ThankYou.vue';

const routes = [
  {
      name: 'home',
      path: '/',
      component: HomeComponent
  },
  {
      name: 'create',
      path: '/create',
      component: CreateComponent
  },
  {
      name: 'posts',
      path: '/posts',
      component: IndexComponent
  },
  {
      name: 'edit',
      path: '/edit/:id',
      component: EditComponent
  },
  {
      name: 'team',
      path: '/team',
      component: TeamComponent
  },
  {
      name: 'question1',
      path: '/question1',
      component: QuestionOne
  },
  {
      name: 'question2',
      path: '/question2',
      component: QuestionTwo
  },
  {
      name: 'question3',
      path: '/question3',
      component: QuestionThree
  },
  {
      name: 'question4',
      path: '/question4',
      component: QuestionFour
  },
  {
      name: 'question5',
      path: '/question5',
      component: QuestionFive
  },
  {
      name: 'question6',
      path: '/question6',
      component: QuestionSix
  },
  {
      name: 'details',
      path: '/details',
      component: EnterDetails
  },
  {
      name: 'thankyou',
      path: '/thankyou',
      component: ThankYou
  }
];

const router = new VueRouter({ mode: 'history', routes: routes});
const app = new Vue(Vue.util.extend({ router }, App)).$mount('#app');

