<!DOCTYPE html>
<html>
	<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Laravel</title>
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&display=swap" rel="stylesheet">
        <link href="{{ mix('css/app.css') }}" type="text/css" rel="stylesheet" />
        <meta name="csrf-token" value="{{ csrf_token() }}" />

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <link rel="stylesheet" href="css/style.css">
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	</head>
<body>

	<!-- Content -->
	<div class="content_container">
		<div class="content">
			<div class="content_full content_twelve">

				<div class="form_container" style="text-align: center; padding: 100px">

					@if (empty($_GET['code']))

						<h3><a href="{{ $authorization }}">Authorize &raquo;</a></h3>

					@else

						<p>Access Token:<br/><strong>{{ $array->access_token }}</strong></p>
						<p>Refresh Token:<br/><strong>{{ $array->refresh_token }}</strong></p>

					@endif

					<a href="{{ url('/') }}">submit data</a>

				</div>
			</div>
		</div>
	</div>
	<!-- Content -->

</body>
</html>