<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width"/>
		<title>Submit sample data</title>
		<link rel="stylesheet" type="text/css" href="style.css" media="screen"/>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	</head>

	<body>

		<form id="sheets" name="sheets" class="form_body">

			<input id="first_name" name="last_name" class="form_field" value="" placeholder="First Name"/>

			<input id="last_name" name="last_name" class="form_field" value="" placeholder="Last Name"/>

			<input id="email" name="email" class="form_field" value="" placeholder="Email Address"/>

			<input id="submit" name="submit" type="button" class="form_button" value="Submit" onClick="submit_form()">

		</form>

		<div class="form_message" id="message"></div>

	</body>

<script>
	
function submit_form() {    
	// Check Fields
    var complete = true;
	var error_color = '#FFD9D9';
    var fields = ['first_name','last_name','email'];
    var row = '';
    var i;
    for(i=0; i < fields.length; ++i) {
        var field = fields[i];
        $('#'+field).css('backgroundColor', 'inherit');
        var value = $('#'+field).val();       
	    // Validate Field
        if(!value) {
            if(field != 'message') {
                $('#'+field).css('backgroundColor', error_color);
                var complete = false;
            }
            } else {            
			// Sheet Data
            row += '"'+value+'",';
        }
    }
   
    // Submission
    if(complete) {		
		// Clean Row
		row = row.slice(0, -1);		
        // Config
        var gs_sid = '1_4JX_sK6S1nsXr7-4r6EWaYStmgsgjN0JYt-5Gt_0ac'; // Enter your Google Sheet ID here
        var gs_clid = '680888018663-t1m25b32v4mrgr5m8ae9fnbmsu2nbikl.apps.googleusercontent.com'; // Enter your API Client ID here
        var gs_clis = '6_IaTgcjI59D5v6Liuls316Y'; // Enter your API Client Secret here
        var gs_rtok = '1/ZYxagH-VWF59PbknBy_p5NX4wrJI0FT6Pz_Swu86_tY'; // Enter your OAuth Refresh Token here
        var gs_atok = false;
        var gs_url = 'https://sheets.googleapis.com/v4/spreadsheets/'+gs_sid+'/values/A1:append?includeValuesInResponse=false&insertDataOption=INSERT_ROWS&responseDateTimeRenderOption=SERIAL_NUMBER&responseValueRenderOption=FORMATTED_VALUE&valueInputOption=USER_ENTERED';
        var gs_body = '{"majorDimension":"ROWS", "values":[['+row+']]}';        
         // HTTP Request Token Refresh
        var xhr = new XMLHttpRequest();
        xhr.open('POST', 'https://www.googleapis.com/oauth2/v4/token?client_id='+gs_clid+'&client_secret='+gs_clis+'&refresh_token='+gs_rtok+'&grant_type=refresh_token');
        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xhr.onload = function() {            
            var response = JSON.parse(xhr.responseText);
            var gs_atok = response.access_token;            
			// HTTP Request Append Data
            if(gs_atok) {
                var xxhr = new XMLHttpRequest();
                xxhr.open('POST', gs_url);
                xxhr.setRequestHeader('Content-length', gs_body.length);
                xxhr.setRequestHeader('Content-type', 'application/json');
                xxhr.setRequestHeader('Authorization', 'OAuth ' + gs_atok );
                xxhr.onload = function() {
					if(xxhr.status == 200) {
						// Success
						$('#message').html('<p>Row Added to Sheet : Response:<br/>'+xxhr.responseText+'</p>');
						} else {
						// Fail
						$('#message').html('<p>Row Not Added</p><p>Response:<br/>'+xxhr.responseText+'</p>');
					}
                };
                xxhr.send(gs_body);
            }            
        };
        xhr.send();
    }
}

</script>


</html>