<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/callback/auth', 'GoogleController@callback');

Route::get('/form/submit', 'GoogleController@sample');

Route::get('/{any}', function () {
  return view('home');
})->where('any', '.*');

